import numpy as np
import random
import matplotlib.pyplot as plt

random.seed(42)
np.random.seed(42)


class Audience_Member():
    def __init__(self, stage, bars, thirst_threshhold=0.7):
        # Stage where the person is at
        self.stage = stage
        # Thirst level (over 1)
        self.thirst = random.uniform(0, thirst_threshhold)
        # Threshhold to be thirsty (over 1)
        self.thirst_threshhold = thirst_threshhold
        # Cooldown time for thirst to start building up (seconds)
        self.cooldown = random.randint(0, 15) * 60
        # Care for the current show at the stage (over 1)
        self.care_w = random.uniform(0, self.thirst_threshhold)
        # Time that the user is willing to wait in queue (seconds)
        self.patience = random.randint(15, 30) * 60
        # Is the person currently in queue
        self.in_queue = False
        # All bars
        self.bars = bars
        # Bar the user is currently at
        self.current_bar = 0

        self.visits_to_bar = 0

    def want_drink(self):
        if self.stage.at_show:
            return self.thirst > self.care_w + self.thirst_threshhold
        else:
            return self.thirst > self.thirst_threshhold

    def update(self):
        if self.in_queue:
            return
        if self.cooldown <= 0:
            self.thirst += 0.0001
        else:
            self.cooldown -= 1

    def iter(self):
        if self.in_queue:
            self.patience -= 1
            if self.patience <= 0:
                self.bars[self.current_bar].leave_queue(self)
                # FIXME: likely a bug source
                next_bar = self.bars[self.current_bar].get_next_bar(self.bars)
                self.current_bar = next_bar.bar_n

                next_bar.new_arrival(self)
                # Reinitialize the patience
                self.patience = random.randint(15, 25) * 60
            return 1

        self.update()
        if self.want_drink():
            # FIXME: does this return the bar corresponding the stage? also, parameters?
            next_bar = self.stage.get_next_bar()
            next_bar.new_arrival(self)
            self.current_bar = next_bar.bar_n
            self.in_queue = True
            self.visits_to_bar += 1
            return 1
        return 0

    def served(self):
        self.thirst = 0
        # Leaves the queue
        self.in_queue = False
        # self.bars[self.current_bar].leave_queue(self)
        self.cooldown = random.randint(45, 60) * 60
        # Reinitialize the patience
        self.patience = random.randint(15, 25) * 60
        self.current_bar = stage.stage_n


class Bar():
    def __init__(self, bar_n, bartenders=1, intercept=1, scale=0.1, weights=[0, 0, 0]):
        # Bar queue
        self.queue = []
        # Parameters of the serving time model
        self.bartenders = bartenders
        self.intercept = intercept
        self.scale = scale
        # Serving time, when the bar will be able to attend newcomers
        self.busy_till = 0
        # Number of the current bar
        self.bar_n = bar_n
        # Chances of a person going to other bars if they are impatient
        self.weights = weights
        self.n_served = 0
        self.histogram = []
        self.serve_times = []

    # TODO: if queue not full (?)
    # Add new person to queue
    def new_arrival(self, audience_member):
        self.queue.append(audience_member)

    # remove audience member from queue
    def leave_queue(self, audience_member):
        self.queue.remove(audience_member)

    def get_next_bar(self, bars):
        next_bar = np.random.choice(bars, p=self.weights)
        return next_bar

    # serve
    def serve(self):
        next_in_line = self.queue.pop()
        # TODO: Add age check
        self.n_served += 1
        next_in_line.served()
        x = (np.random.exponential(scale=self.scale) +
             self.intercept) * 60 / self.bartenders
        self.serve_times.append(x)
        return x

    def iter(self, current_t):
        if len(self.queue) > 0 and not (self.busy_till >= current_t):
            self.busy_till = current_t + self.serve()

        self.histogram.append(len(self.queue))


class Stage():
    def __init__(self, stage_n, bars, offset=0, show_duration=45*60, break_duration=15*60, num_of_shows=5):
        self.stage_n = stage_n
        self.offset = offset
        self.show_duration = show_duration
        self.break_duration = break_duration
        self.num_of_shows = num_of_shows
        self.current_show = 0
        self.at_show = False
        self.t_next_event = break_duration + self.offset
        self.next_bars = bars

        self.histogram = []

    # Get the next bar to stage with the probabilitys according to the distance
    def get_next_bar(self):
        # FIXME: works if bars passed in order, but should be a parameter
        next_bar = np.random.choice(self.next_bars, p=[0.7, 0.2, 0.1])
        return next_bar

    # Update if show is on or not
    def iter(self, current_time):

        if self.t_next_event == current_time and self.num_of_shows >= 0:
            # On break, next event after the break
            if self.at_show:
                self.t_next_event += self.break_duration
            # On show, next event
            else:
                self.t_next_event += self.show_duration
                self.num_of_shows -= 1
            self.at_show = not self.at_show
        self.histogram.append(int(self.at_show))


bars = []
n_bars = 3
n_audience = 1000

# We assume that 1 bartender takes about 1 minute to serve an
# audience member most of the time. Therfore two bartender take 0.5 minutes
# and 10 bartender take 0.1 minute the best case senario is modeld as the intercept
# of the exponetial distribution


b1 = Bar(0, bartenders=3, weights=[0, 0.6, 0.4])
b2 = Bar(1, bartenders=2, weights=[0.6, 0, 0.4])
b3 = Bar(2, bartenders=2, weights=[0.4, 0.6, 0])
bars = [b1, b2, b3]

s1 = Stage(0, offset=0, bars=[b1, b2, b3])
s2 = Stage(1, offset=20*60, bars=[b2, b3, b1])
s3 = Stage(2, offset=40*60, bars=[b3, b2, b1])
stages = [s1, s2, s3]

audience = []
for i in range(n_audience):
    stage = np.random.choice(stages, p=[0.6, 0.3, 0.1])
    audience.append(Audience_Member(stage, bars))

final_time = 45 * 60 + (15 + 45) * 5 * 60
not_over = True
wait = 0


wait = 0
for t in range(final_time):

    s1.iter(t)
    s2.iter(t)
    s3.iter(t)

    b1.iter(t)
    b2.iter(t)
    b3.iter(t)

    for a in audience:
        wait += a.iter()


f, (ax11, ax12, ax13) = plt.subplots(1, 3)

ax11.plot(b1.histogram, label="Queue Bar 1", color="blue")
ax11.set_title("Queue Bar 1")
ax11.plot(s1.histogram, label="Stage 1 performances", color="grey")
ax11.legend()
ax11.set_ylim([0, max(b1.histogram)])

ax12.plot(b2.histogram, label="Queue Bar 2", color="orange")
ax12.set_title("Queue Bar 2")
ax12.plot(s2.histogram, label="Stage 2 performances", color="grey")
ax12.legend()
ax12.set_ylim([0, max(b1.histogram)])

ax13.plot(b3.histogram, label="Queue Bar 3", color="green")
ax13.set_title("Queue Bar 3")
ax13.plot(s3.histogram, label="Stage 3 performances", color="grey")
ax13.legend()
ax13.set_ylim([0, max(b1.histogram)])

print("METRICS:")
print()
print("Average waiting time:", (wait/n_audience)/60, "mins")
print("Average clients queue Bar 1:", sum(b1.histogram)/final_time)
print("Average clients queue Bar 2:", sum(b2.histogram)/final_time)
print("Average clients queue Bar 3:", sum(b3.histogram)/final_time)
print("Average clients in system (total):", sum(
    b1.histogram + b2.histogram + b3.histogram)/final_time)
print()
print(
    "Effective Total Arrival Rate (total)",
    sum([a.visits_to_bar for a in audience])
)
print(
    "Effective Total Service Rate (total)",
    sum(b1.serve_times + b2.serve_times + b3.serve_times) /
    (len(b1.serve_times)+len(b2.serve_times)+len(b3.serve_times)))
print("Bars occupance ratio", )

plt.show()
