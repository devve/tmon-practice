## Description

In june 2023 there will be a festival in Madrid. But... How many bartenders
will be needed? How much beer will be needed so they don't run out?

The festival will have three (3) stages. Stage 1 is bigger than Stage 2, which is bigger than Stage 3. Each stage will have a bar assignated, so there are
three bars.

Concerts at each stage start at different times and have the same length (1h),
so there is only one stage empty at once (the other two have a concert
ongoing). This will model our arrival distribution, since people will mostly
go for a beer while no concert on their current stage.

Some people might want to go to the next bar if too long on the current bar.

### Floor Plan
![Situational Schema](images/situational-schema.png)

### Audience Member
![Flow-Chart Audience](images/audience_member.png)
### Bartender
![Flow-Chart Bartender](images/bartender.png)

## Questions

* What amount of beer is going to be needed?
* How many bartenders will be needed per bar? (some limitation on ppl/sqr/mtr)

## Considerations
* Different service points (3 bars f.i).
* If queue is too long, the person may leave (there needs to be a random
variable).
* A person who just got a beer is more likely to get another one.
* Consider consumption rhythm of people (some drink fast and a lot and others
don't).
* Dependency between arrival times: Customers are more likely to appear after
a concert is finished.

> To get it more complex we could initialize people with a limited amount of
money.
